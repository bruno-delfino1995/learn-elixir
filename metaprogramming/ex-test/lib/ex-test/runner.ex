defmodule ExTest.Runner do
    def run(tests, module) do
      tests
      |> Enum.each(&(run_test(&1, module)))
    end

    defp run_test({name, _description} = test_spec, module) do
      try do
        apply(module, name, [])
        [:green, "."] |> IO.ANSI.format |> IO.puts
      rescue
        ex in ExTest.Assertion.AssertionError -> print_error(ex, test_spec, module)
      end
    end

    defp print_error(exception, {_name, description}, module) do
      messages = [
        [:red, "Test failed for #{module} - #{description}"],
        [:red, "\t#{exception.message}"]
      ]

      pipeline = &(&1 |> IO.ANSI.format |> IO.puts)

      messages
      |> Enum.each(pipeline)
    end
  end
