defmodule ExTest do
  defmacro __using__(_opts) do
    quote do
      @before_compile unquote(__MODULE__)

      @tests []

      import ExTest, only: [test: 2]
      import ExTest.Assertion
    end
  end

  defmacro __before_compile__(_env) do
    quote do
      def run(), do: ExTest.Runner.run(@tests, __MODULE__)
    end
  end

  defmacro test(description, do: body) do
    name = String.to_atom(description)

    quote do
      @tests [{unquote(name), unquote(description)} | @tests]
      def unquote(name)(), do: unquote(body)
    end
  end
end
