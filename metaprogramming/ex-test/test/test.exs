defmodule Test do
  use ExTest

  test "Comparisions" do
    assert 1 == 1
    assert 1 < 2
    assert 2 > 1
    assert 5 <= 5
    assert 4 <= 3
    assert "bla" === "bla"
    assert %{a: 1} === %{a: 1}
  end
end
