defmodule Template do
  import Html

  def render do
    markup do
      tag :table do
        tag :tr do
          for i <- 0..5 do
            tag :td, do: text("Cell #{i}")
          end
        end
      end
      tag :div do
        text "Some Nested Content"
      end
    end
  end

  def render_with_tags do
    markup do
      table do
        tr do
          for i <- 0..5 do
            td do: text("Cell #{i}")
          end
        end
      end
      div do
        text "Some Nested Content"
      end
    end
  end

  def render_with_attrs do
    markup do
      table id: "tb1" do
        tr do
          for i <- 0..5 do
            text "Cell #{i}"
          end
          td class: "td" do
            text "Inner content"
          end
          td class: "td", do: text "Inline content"
        end
      end
      div do
        text "Some Nested Content"
      end
    end
  end

  def analyse_ast do
    markup do
      p id: "p" do
        text "Paragraph content"
      end
      div do
        text "Some Nested Content"
      end
      p id: "p2", do: text("Single line paragraph")
    end
  end
end

IO.puts Template.render()
IO.puts Template.render_with_tags()
IO.puts Template.render_with_attrs()
IO.puts Template.analyse_ast()
