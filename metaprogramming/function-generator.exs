defmodule Times do
  defmacro times_n(quantity) do
    {result, _} = Code.eval_quoted(quantity)
    name = :"times_#{result}"

    quote do
      def unquote(name)(number) do
        number * unquote(quantity)
      end
    end
  end
end

defmodule Func do
  defmacro echo(name) do
    IO.inspect name

    ast = quote bind_quoted: [name: name] do
      IO.inspect "Defining the echo function with #{name}"

      def unquote(name)(), do: unquote(name)
    end

    IO.puts Macro.to_string(ast)
    ast
  end

  defmacro puts(str) do
    ast = quote bind_quoted: [string: str] do
      IO.puts string
    end

    IO.inspect ast
  end

  defmacro allow_unquote(name) do
    IO.inspect name
    escaped_name = Macro.escape(name, unquote: true)

    IO.puts "Can use unquote inside macro parameters #{inspect escaped_name}"
    quote do
      IO.puts unquote(escaped_name)
    end
  end
end

defmodule Test do
  require Times

  Times.times_n(3)
  Times.times_n(4)
  Times.times_n(5 + 5)

  require Func

  Func.echo(:martin)
  [:valim, :armstrong] |> Enum.each(&Func.echo/1)

  name = :jose
  Func.allow_unquote(unquote(name))

  name = :someone
  def unquote(name)(), do: unquote(name)

  Func.puts "Hi bind_quoted"
end

IO.puts "\n# Start testing\n"
IO.puts Test.times_3(4) #=> 12
IO.puts Test.times_4(5) #=> 20
IO.puts Test.times_10(5) #=> 50

IO.puts Test.martin()
IO.puts Test.valim()
IO.puts Test.armstrong()

IO.puts Test.someone()
