defmodule MacroEvaluation do
  defmacro inspect_param(param), do: IO.puts "Your param is #{inspect param}"

  defmacro inject_code(code) do
    IO.inspect code
    quote do: IO.puts("World")
  end

  # In order to insert the code itself and not the identifier code in our code,
  # we have to use unquote (only allowed inside quote). This is necessary because
  # quoted expressions don't know about the outside word and insert everything that
  # you tell them literally. The quote's block is inserted as it's, without variable
  # bounding, we have the bounds only when defining the quote, after inserting it where
  # the macros is being used the bound gone. So because of that we get an unespecified
  # identifier for code, because it's not defined anywhere. But we want to kind of
  # evaluate the code and insert the content of it in our macro, in order to do that
  # we have to unquote the code (a quoted expression), and insert its content in the
  # body of the macro. Unquote is like string interpolation, when we write "sum = #{1 + 2}"
  # Elixir will parse the expression and insert its result, unquote does almost the same
  # it will parse the quoted expression and turns it into code to be executed when the
  # code that macro injected is executed
  defmacro inject_block(do: code) do
    quote do
      IO.puts "Your code will be called"
      return = unquote(code)
      IO.puts("Your code has returned #{return}")
    end
  end
end

defmodule Scope do
  defmacro update_local(value) do
    local = "Some value"

    result = quote do
      local = unquote(value)

      # As local is in the quoted code scope we don't have to unquote it
      IO.puts "End of macro body => #{local}"
    end

    IO.puts "In macro definition => #{local}"

    result
  end

  defmacro change_local(value) do
    quote do
      var!(local) = unquote(value)
    end
  end
end

# We have to use another module because Elixir has to first compile the file and
# then run it. Whether we use the module in the same file Elixir will not know how
# to proceed because it has to compile and load the first module before running the code.
# Also we are using macros, which enforces the load of the first module before starting
# the evaluation of the code that uses its macros
defmodule Test do
  import MacroEvaluation

  # When Elixir is evaluating the code and find a macro, the parameters aren't
  # evaluated, instead of that they are quoted and sent to the macro as tuples
  inspect_param :atom
  inspect_param 1
  inspect_param 1.0
  inspect_param [1, 2, 3]
  inspect_param "binary"

  inspect_param {1, 2}
  inspect_param {1, 2, 3, 4, 5}

  inspect_param do: 1
  inspect_param do 1 end
  inspect_param do: (a = 1; a + a)
  inspect_param do
    1 + 2
  else
    3 + 4
  end

  # Everything in Elixir is an expression, while compiling Elixir is evaluating
  # the expressions, if we put a code that outputs something, the code will be
  # be evaluated and the string will be send to our screen. If this macro returns
  # some quoted expression it will be put in our module and evaluated, lets say
  # that this macro is `quote do: IO.puts("World")`, after evaluating that macro
  # it will return the quoted expression that will be evaluated and will print World
  # in our screen
  inject_code IO.puts("hello")
  # The same as above, without the inspect in the macro
  IO.puts("World")

  inject_block do
    IO.puts "This is the code to be inserted"
    0
  end

  require Scope

  local = 123
  # A macro has its own scope, so definition on it won't mess the outside world
  Scope.update_local "dog"
  IO.puts "On return => #{local}"

  # But as you can see in this macro we can use var! to change a outside variable
  Scope.change_local "cat"
  IO.puts "Real change using var! => #{local}"

  # The representation of code in Elixir is a tuple containing {operator, metainfo,
  # parameters}, by so we can say that Elixir is a homoiconic language, as we can
  # represent the internal language using its own data structure. Haing such
  # knowledge allow us to create code on the fly to be analyzed by Code.eval_quoted
  IO.inspect (Code.eval_quoted quote(do: 1 + 1))
  IO.inspect (Code.eval_quoted {:+, [], [1, 1]})
end
