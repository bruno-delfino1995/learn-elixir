defmodule Hub do
  @moduledoc """
  Repo mapping for given username
  """
  @username "bruno-delfino1995"

  HTTPotion.start

  "https://api.github.com/users/#{@username}/repos"
  |> HTTPotion.get(["User-Agent": "Elixir"])
  |> Map.get(:body)
  |> Poison.decode!()
  |> Enum.each(fn repo ->
    def unquote(String.to_atom(repo["name"]))() do
      unquote(Macro.escape(repo))
    end
  end)
end
