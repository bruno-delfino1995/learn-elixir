#!/bin/sh

set -e

function acbuildend {
    acbuild --debug end
    exit $1
}

trap acbuildend EXIT

acbuild --debug begin quay.io/coreos/alpine-sh

# Add repositories to apk
acbuild --debug run -- /bin/sh -c "echo http://dl-cdn.alpinelinux.org/alpine/v3.5/main >> /etc/apk/repositories"
acbuild --debug run -- /bin/sh -c "echo http://dl-cdn.alpinelinux.org/alpine/v3.5/community >> /etc/apk/repositories"

# Install language and support things
acbuild --debug run -- apk upgrade
acbuild --debug run -- apk update
acbuild --debug run -- apk add build-base git elixir nodejs-current erlang-crypto erlang-syntax-tools erlang-parsetools
acbuild --debug run -- npm install -g yarn

# Phoenix and other mix things
acbuild --debug run -- mix local.hex --force
acbuild --debug run -- mix local.rebar --force
acbuild --debug run -- mix archive.install --force https://github.com/phoenixframework/archives/raw/master/phoenix_new.ez

# Working directory
acbuild --debug set-working-directory /home/app

# Copy dependencies
acbuild --debug copy-to-dir mix.exs mix.lock package.json yarn.lock ./
acbuild --debug run -- mix do deps.get, compile
acbuild --debug run -- yarn install

acbuild --debug copy-to-dir ./ ./

acbuild --debug set-exec mix phoenix.server
acbuild --debug write discuss.aci
acbuild --debug end
