sum = 0
list = [1, 2, 3, 4, 5]

for n <- list do
    sum = sum + n
end

IO.puts "The sum is #{sum}"

Enum.each [-1, 2, -3, 4], fn elem ->
  unless elem < 0 do
    sum = sum + elem
  end
end

IO.puts "The sum is #{sum}"

# In Elixir, variables are captured by value. When we refer to sum in the anonymous
# function or in the loop, we are referring to the value that sum had at the time
# we created the function.
# http://elixir-lang.readthedocs.io/en/latest/technical/scoping.html
