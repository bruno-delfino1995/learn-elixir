[3 | []] = [3]
[2 | [3 | []]] = [2, 3]
[1 | [2 | [3 | []]]] = [1, 2, 3]

defmodule MyList do
    def leng([]), do: 0
    def leng([_head | tail]), do: 1 + leng(tail)

    def square([]), do: []
    def square([head | tail]), do: [head * head | square(tail)]

    def add_1([]), do: []
    def add_1([head | tail]), do: [head + 1 | add_1(tail)]

    def map([], _func), do: []
    def map([head | tail], func), do: [func.(head) | map(tail, func)]

    def sum([]), do: 0
    def sum([head | tail]), do: head + sum(tail)

    def mapsum([], _func), do: 0
    def mapsum([head | tail], func), do: func.(head) + mapsum(tail, func)

    def max([x]), do: x
    def max([head | tail]), do: Kernel.max(head, max(tail))

    def caesar([], _n), do: []
    def caesar([head | tail], n) when head + n <= ?z, do: [head + n | caesar(tail, n)]
    def caesar([head | tail], n), do: [head + n - 26 | caesar(tail, n)]

    def span(from, to) when from > to, do: []
    def span(from, to), do: [from | span(from + 1, to)]
end
