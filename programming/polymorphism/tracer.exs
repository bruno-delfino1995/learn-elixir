defmodule Tracer do
  def dump_args(args), do: args |> Enum.map(&inspect/1) |> Enum.join(", ")

  def dump_defn(name, args), do: "#{name}(#{dump_args(args)})"

  defmacro def({:when, _, [head = {name, _, args}, guard]}, do: content) do
    quote do
      Kernel.def unquote(head) when unquote(guard) do
        IO.puts "==> call: #{Tracer.dump_defn(unquote(name), unquote(args))} "
        result = unquote(content)
        IO.puts "<== result: #{result}"
        result
      end
    end
  end

  defmacro def(definition = {name, _line, args}, do: content) do
    quote do
      Kernel.def unquote(definition) do
        # Here we have access to the runtime values for arguments because we are
        # putting the name of them on tracer call, and the name for each arg is a
        # bind with its runtime value, so the values are passed to our function and
        # we are able to evaluate it
        IO.puts "==> call: #{Tracer.dump_defn(unquote(name), unquote(args))} "
        result = unquote(content)
        IO.puts "<== result: #{result}"
        result
      end
    end
  end

  defmacro __using__(_options) do
    quote do
      import Kernel, except: [def: 2]
      import unquote(__MODULE__), only: [def: 2]
    end
  end
end

defmodule Test do
  use Tracer

  def puts_sum_three(a, b, c), do: IO.inspect(a + b + c)

  def add_list(list), do: Enum.reduce(list, 0, &(&1 + &2))

  def adult?(age) when age >= 18 and age <= 60, do: true
  def adult?(age), do: false
end

Test.puts_sum_three(1, 2, 3)
Test.add_list([5, 6, 7, 8])
Test.adult?(21)
Test.adult?(12)
