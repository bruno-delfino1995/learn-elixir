defmodule KT do
    def repeat(_func, 0), do: []
    def repeat(func, times), do: [func.()] ++ repeat(func, times - 1)

    def repeat_with_index(_func, 0), do: []
    def repeat_with_index(func, times), do: [func.(times)] ++ repeat_with_index(func, times - 1)

    def sum([]), do: 0
    def sum([head | tail]), do: head + sum(tail)

    def sum([], accum), do: accum
    def sum([head | tail], accum), do: sum(tail, accum + head)

    def reduce([], accum, _func), do: accum
    def reduce([head | tail], accum, func), do: reduce(tail, func.(accum, head), func)

    def reduce_right([], accum, _func), do: accum
    def reduce_right(list, accum, func) do
      list = Enum.reverse(list)
      func = &(func.(&2, &1))

      reduce(list, accum, func)
    end

    def filter([], _func), do: []
    def filter([head | tail], func), do: (if not func.(head), do: filter(tail, func), else: [head] ++ filter(tail, func))

    def average([]), do: 0
    def average(list), do: reduce(list, 0, &(&1 + &2)) / length(list)

    def is_odd(0), do: false
    def is_odd(number), do: is_even(number - 1)

    def is_even(0), do: true
    def is_even(number), do: is_odd(number - 1)

    def curry(func) do
        {_, arity} = :erlang.fun_info(func, :arity)
        _curry(func, arity, [])
    end

    defp _curry(func, 0, args) do
        apply(func, args)
    end

    defp _curry(func, arity, args) do
        fn arg -> _curry(func, arity - 1, args ++ [arg]) end
    end

    def memoize(_func) do
        ~S"""
        Without macros it's impossible in Elixir, once it depends on name and arity.
        Since we don't have varargs in Elixir, that makes it impossible.
        """
    end
end
