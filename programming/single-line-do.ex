defmodule Times, do: (
    def double(number), do: number * 2
)
