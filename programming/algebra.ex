defmodule Algebra do
    def sum_until(1), do: 1
    def sum_until(n), do: n + sum_until(n - 1)

    def gcd(x, 0), do: x
    def gcd(x, y), do: gcd(y, rem(x, y))
end
