defmodule Factorial do
    def of(0), do: 1
    def of(n), do: n * of(n - 1)

    def of_tc(n) when n >= 0, do: do_factorial(n, 1)
    defp do_factorial(0, acum), do: 1 * acum
    defp do_factorial(n, acum), do: do_factorial(n - 1, acum * n)
end
