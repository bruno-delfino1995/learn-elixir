defmodule MultiMap do
  @moduledoc """
  Manage a simple map which accepts multiple values for the same key
  """

  @doc """
  Create a new map

  ## Examples

  ```
  iex> MultiMap.new
  %{}
  ```
  """
  def new do
    %{}
  end

  @doc """
  Add a new get to the map

  ## Examples

  ```
  iex> map = MultiMap.new() |> MultiMap.put({2013, 12, 19}, "Dentist")
  %{{2013, 12, 19} => ["Dentist"]}
  iex> MultiMap.put(map, {2013, 12, 19}, "Shopping")
  %{{2013, 12, 19} => ["Shopping", "Dentist"]}
  ```
  """
  def put(map, key, val) do
    Map.update(map, key, [val], &[val | &1])
  end

  @doc """
  Get the entry for the given key

  ## Examples

  ```
  iex> list = MultiMap.new()
  iex> |> MultiMap.put({2013, 12, 19}, "Dentist")
  iex> |> MultiMap.put({2013, 12, 20}, "Shopping")
  iex> |> MultiMap.put({2013, 12, 19}, "Movies")
  iex> MultiMap.get(list, {2013, 12, 19})
  ["Movies", "Dentist"]
  iex> MultiMap.get(list, {2013, 12, 20})
  ["Shopping"]
  iex> MultiMap.get(list, {2013, 12, 21})
  []
  ```
  """
  def get(list, key), do: Map.get(list, key, [])
end
