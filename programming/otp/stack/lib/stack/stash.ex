defmodule Stack.Stash do
  use GenServer

  # External API
  #######################################

  def start_link(initial_state) do
    {:ok, _pid} = GenServer.start_link(__MODULE__, initial_state)
  end

  def get(pid) do
    GenServer.call(pid, :get)
  end

  def set(pid, state) do
    GenServer.cast(pid, {:set, state})
  end

  # GenServer Implementation
  #######################################

  def handle_call(:get, _from, state) do
    {:reply, state, state}
  end

  def handle_cast({:set, state}, _current_state) do
    {:noreply, state}
  end
end
