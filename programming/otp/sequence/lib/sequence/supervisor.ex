defmodule Sequence.Supervisor do
  use Supervisor

  def start_link(initial_number) do
    result = {:ok, master_sup} = Supervisor.start_link(__MODULE__, nil)

    start_workers(master_sup, initial_number)

    result
  end

  def start_workers(sup_pid, initial_number) do
    {:ok, stash} = Supervisor.start_child(sup_pid, worker(Sequence.Stash, [initial_number]))
    {:ok, _server} = Supervisor.start_child(sup_pid, supervisor(Sequence.SubSupervisor, [stash]))
  end

  def init(_) do
    supervise([], strategy: :one_for_one)
  end
end
