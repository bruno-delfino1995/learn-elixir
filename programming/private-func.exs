defmodule Private do
  # Private functions can't be captured using the module prefix
  def error(x), do: Private.func(x)
  def success(x), do: func(x)

  defp func(x), do: IO.puts(x)
end

Private.error('hay') # => function Private.func/1 is undefined of private
Private.success('hay')
