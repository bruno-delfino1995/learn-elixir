# Execute this with elixir -r "struct.ex" "struct.exs"

m = %{a: 1, b: 2, c: 3}
new_m = %{m | a: 4} # Update operator - only for already defined properties
IO.inspect m
IO.inspect new_m
IO.inspect Map.put_new(m, :d, 4)

IO.puts "The a for m is #{m.a}, and for new_m is #{new_m[:a]}"

x = %Subscriber{}
%{name: "Someone"} = x

IO.puts "Hello my name is #{x.name}" # However we cannot use the square braces notation

IO.inspect %{x | name: "John"}
IO.inspect %Subscriber{x | age: 18}
IO.inspect %Subscriber{x | name: 12} # Even it being a struct, the types aren't checked

y = %Subscriber{name: "Someone", age: 18, type: "Employee"}
%Subscriber{age: 18} = y

_z = %{name: "Someone", age: 18, type: "Employee"}
# %Subscriber{age: 18} = z # Don't match because of the "types"
