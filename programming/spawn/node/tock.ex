defmodule Tock do
  def start(ticker) do
    pid = spawn(__MODULE__, :receiver, [])

    # As the ticker pid is registered globally we need to connect
    # in the node that the ticker process was spawned. If you try
    # to register without connecting to the node you will get an
    # error saying that the ticker pid is :undefined
    ticker.register(pid)
  end

  def receiver do
    receive do
      {:tick} ->
        IO.puts "Tock in client"
    end

    receiver()
  end
end
