defmodule RingTicker do
  @name :ring_ticker

  def start do
    pid = spawn(__MODULE__, :generator, [[]])
    :global.register_name(@name, pid)
  end

  def register(client_pid) do
    IO.puts "Register #{inspect client_pid}"
    generator_pid = :global.whereis_name(@name)
    send(generator_pid, {:register, client_pid})
  end

  def generator(clients) do
    receive do
      {:register, pid} ->
        IO.puts "Registering #{inspect pid}"
        clients = register_new_client(clients, pid)
        generator(clients)
    end
  end

  defp register_new_client([], client) do
    send_ack(client, client)

    [client]
  end
  defp register_new_client(clients, client) do
    first = List.first(clients)
    last = List.last(clients)

    send_ack(client, first)
    send(last, {:change_next, client})

    clients ++ [client]
  end

  defp send_ack(client, next), do: send(client, {:registered, next})
end
