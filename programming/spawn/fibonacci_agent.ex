defmodule FibonnaciAgent do
  def start_link do
    cache = Enum.into [{0, 0}, {1, 1}], Map.new
    Agent.start_link(fn -> cache end)
  end

  def fib(agent, number) do
    Agent.get_and_update(agent, &do_fib(&1, number))
  end

  defp do_fib(cache, number) do
    if cached = cache[number] do
      {cached, cache}
    else
      {result, new_cache} = do_fib(cache, number - 1)
      result = result + new_cache[number - 2]
      {result, Map.put(new_cache, number, result)}
    end
  end
end
