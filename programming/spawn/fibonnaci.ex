defmodule Fibonnaci do
  def of(0), do: 0
  def of(1), do: 1
  def of(n), do: of(n - 1) + of(n - 2)

  def client(scheduler) do
    send(scheduler, {:ready, self()})

    receive do
      {:process, {args, n}} ->
        result = apply(Fibonnaci, :of, args)
        send(scheduler, {:result, n, result})

        client(scheduler)
      {:shutdown} ->
        exit(:normal)
    end
  end
end
