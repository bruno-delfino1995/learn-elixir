defmodule ProcessesLinking do
  def exit_after(process_id, message) do
    send(process_id, message)
    exit(:boom)
  end

  def raise_after(process_id, message) do
    send(process_id, message)
    raise RuntimeError, message: "It's a proposital error"
  end

  @doc """
  When processes are linked using spawn_link the parent process will die if the
  child dies with an error of exit with an error code. Each of them receive
  messages when the other exits, and if the child exists abnormally, it kills
  the others.
  But if you want to trap the exits and convert it into messages, without killing
  the parent process you can use Process.flag(:trap_exit, true)
  """
  def link(method) do
    spawn_link(ProcessesLinking, method, [self(), "From parent"])

    :timer.sleep(500)

    wait_for_messages()
  end

  @doc """
  Monitoring a process is different of linking, using process links each process
  on any side of the link will receive a message when the other exits, it's a two
  way message. With monitoring, only the parent process will receive a message about
  its child, e. g., when the child dies the parent will receive a :DOWN message
  """
  def monitor(method) do
    spawn_monitor(ProcessesLinking, method, [self(), "From parent"])

    :timer.sleep(500)

    wait_for_messages()
  end

  def wait_for_messages do
    receive do
      message ->
        IO.puts("Received #{inspect(message)} at main")
        wait_for_messages()
    after 1000 ->
      IO.puts("Nothing happened as far as I am concerned")
    end
  end
end
