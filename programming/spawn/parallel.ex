defmodule Parallel do
  @doc """
  Convert the enumerable of X elements in a new list of pids and then fetch
  the messages from the current process mailbox ordered using the pid of the
  current process in the list
  """
  def pmap(collection, func) do
    me = self()

    collection
    |> Enum.map(fn(el) ->
      spawn_link(fn -> send(me, {self(), func.(el)}) end)
    end)
    |> Enum.map(fn(pid) ->
      receive do {^pid, result} -> result end
    end)
  end
end
