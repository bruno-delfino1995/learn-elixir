defmodule TableFormatterTest do
  use ExUnit.Case
  doctest Issues.TableFormatter
  alias Issues.TableFormatter, as: TF

  def headers, do: [:c1, :c2, :c4]

  def rows_data do
    [
      [c1: "r1 c1", c2: "r1 c2", c3: "r1 c3", c4: "r1+++c4"],
      [c1: "r2 c1", c2: "r2 c2", c3: "r2 c3", c4: "r2 c4"],
      [c1: "r3 c1", c2: "r3 c2", c3: "r3 c3", c4: "r3 c4"],
      [c1: "r4 c1", c2: "r4 c2", c3: "r4 c3", c4: "r4 c4"]
    ]
  end

  def columns_data do
    [
      ["r1++c1", "r2 c1", "r3 c1", "r4 c1"],
      ["r1 c2", "r2 c2", "r3 c2", "r4 c2"],
      ["r1 c4", "r2 c4", "r3+++c4", "r4 c4"],
    ]
  end

  test "split into columns by header" do
    columns = TF.split_into_columns(rows_data(), headers())

    assert length(headers()) == length(columns)
    assert List.first(columns) == ["r1 c1", "r2 c1", "r3 c1", "r4 c1"]
    assert List.last(columns) == ["r1+++c4", "r2 c4", "r3 c4", "r4 c4"]
  end

  test "calculate columns width by longest string" do
    columns_width = TF.widths_of(columns_data())

    assert columns_width == [6, 5, 7]
  end

  test "format according to widths" do
    assert TF.format_for([2, 3, 6], " | ") == "~-2s | ~-3s | ~-6s"
  end

  test "table is correctly converted" do
    table = TF.table_for_columns(rows_data(), headers())

    columns_format = "~-5s | ~-5s | ~-7s"
    separator_format = "~-5s-+-~-5s-+-~-7s"
    separators = ["-----", "-----", "-------"]

    assert List.first(table) == {headers(), columns_format}
    assert elem(List.pop_at(table, 1), 0) == {separators, separator_format}
    assert List.last(table) == {["r4 c1", "r4 c2", "r4 c4"], columns_format}
  end
end
