defmodule CLITest do
  use ExUnit.Case

  import Issues.CLI, only: [parse_args: 1,
    sort_into_ascending_order: 1, convert_to_list_of_hashdicts: 1]

  test ":help returned by option parsing with -h or --help options" do
    assert parse_args(["-h", "anything"]) == :help
    assert parse_args(["--help", "something"]) == :help
  end

  test "three values are returned if three given" do
    assert parse_args(["someone", "something", "5"]) == {"someone", "something", 5}
  end

  test "count is default if two arguments given" do
    assert parse_args(["someone", "something"]) == {"someone", "something", 4}
  end

  test "sort ascending orders the correct way" do
    sorted_list = ["c", "a", "b"]
    |> created_at_list()
    |> sort_into_ascending_order()
    |> Enum.map(fn el -> el["created_at"] end)

    assert sorted_list == ~w(a b c)
  end

  defp created_at_list(values) do
    values
    |> Enum.map(fn el -> [{"created_at", el}, {"other_data", "xxxx"}] end)
    |> convert_to_list_of_hashdicts()
  end
end
