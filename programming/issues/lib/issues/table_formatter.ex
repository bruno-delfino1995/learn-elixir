defmodule Issues.TableFormatter do
  @doc """
  Takes a list of row data, where each rows is a Map, and a list of headers.
  Returns a list of `{fields, format}` to be used by :io.format. The result just
  have to be iterated and printed to STDOUT to get the expected result.

  We calculate the width of each column to fit the longest element in that column
  """
  def table_for_columns(rows, headers) do
    data_by_columns = split_into_columns(rows, headers)

    headers_as_printables = Enum.map(headers, &printable/1)
    columns_width = widths_of(merge(headers_as_printables, data_by_columns))
    columns_format = format_for(columns_width, " | ")
    separator_format = format_for(columns_width, "-+-")
    separators = separator_list(columns_width, "-")

    heading = [{headers, columns_format}, {separators, separator_format}]
    rows_data = data_by_columns |> List.zip() |> Enum.map(&Tuple.to_list/1)

    heading ++ Enum.map(rows_data, &{&1, columns_format})
  end

  @doc """
  Given a list of rows, where each row contains a map list of columns, return a
  list ocontaining lists of the data in each column. The `headers` parameter contains
  the list of columns to extract

  ## Example
  ```
  > list = [
    Enum.into([{"a", "1"}, {"b", "2"}, {"c", "3"}], Map.new),
    Enum.into([{"a", "4"}, {"b", "5"}, {"c", "6"}], Map.new)
  ]
  > Issues.TableFormatter.split_into_columns(list, ["a", "b", "c"])
  [["1", "4"], ["2", "5"], ["3", "6"]]
  ```
  """
  def split_into_columns(rows, headers) do
    for header <- headers do
      for row <- rows, do: printable(row[header])
    end
  end

  @doc """
  Return a binary (string) version of the parameter

  ## Example
  ```
  > Issues.TableFormatter.printable("a")
  "a"
  > Issues.TableFormatter.printable(99)
  "99"
  ```
  """
  def printable(str) when is_binary(str), do: str
  def printable(str), do: to_string(str)

  def merge(head, tail), do: do_merge(head, tail, [])
  defp do_merge([], [], result), do: result
  defp do_merge([head | tail], [append | rest], result) do
    new_result = Enum.concat(result, [to_list(head) ++ append])

    do_merge(tail, rest, new_result)
  end

  defp to_list(list) when is_list(list), do: list
  defp to_list(elem), do: [elem]

  @doc """
  Given a list containing sublists, where each sublist contains the data for a
  column, return a list containing the maximum width of each column

  ## Example
  ```
  > data = [["cat", "wombat", "elk"], ["mongoose", "ant", "gnu"]]
  > Issues.TableFormatter.widths_of(data)
  [6, 8]
  ```
  """
  def widths_of(columns) do
    for column <- columns do
      column |> Enum.map(&String.length/1) |> Enum.max()
    end
  end

  @doc """
  Return a format string that hard codes the widths of a set of columns. We put
  the `joiner` between each column

  ## Example
  ```
  > widths = [5, 6, 99]
  > Issues.TableFormatter.format_for(widths, " + ")
  "~-5s + ~-6s + ~-99s"
  ```
  """
  def format_for(columns_width, joiner) do
    Enum.map_join(columns_width, joiner, fn(width) -> "~-#{width}s" end)
  end

  @doc """
  Generate a list with the character repeated `n` times represented by the perspective
  number in the given list

  ## Example
  > widths = [2, 3, 5]
  > Issues.TableFormatter.separator_list(widths, "+")
  ["++", "+++", "+++++"]
  """
  def separator_list(columns_width, char) do
    Enum.map(columns_width, &String.duplicate(char, &1))
  end
end
