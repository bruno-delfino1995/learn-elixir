defmodule Issues.GithubIssues do
  require Logger

  @github_url Application.get_env(:issues, :github_url)

  def fetch(user, project) do
    Logger.info("Fetching user #{user}'s project #{project}")
    issues_url(user, project)
    |> HTTPoison.get()
    |> handle_response()
    |> parse_body()
  end

  def issues_url(user, project) do
    "#{@github_url}/repos/#{user}/#{project}/issues"
  end

  def handle_response({_, %{status_code: 200, body: body}}) do
    Logger.info("Successful response")
    Logger.debug(fn -> IO.inspect(body) end)
    {:ok, body}
  end
  def handle_response({_, %{status_code: status, body: body}}) do
    Logger.info("Error #{status} returned")
    {:error, body}
  end

  def parse_body({status, body}), do: {status, :jsx.decode(body)}
end
